defmodule Lighttel.Packet.PutPublishHandle do
  @behaviour Lighttel.Packet

  defstruct handles: %{}

  def type do
    20
  end

  def from_payload(
        <<handle_id::big-16, ep_len::big-16, ep::binary-size(ep_len), rest::binary>>,
        opts
      ) do
    rest_handles = from_payload(rest, opts)

    handles =
      %{handle_id => ep}
      |> Map.merge(rest_handles.handles, fn _k, _v1, v2 -> v2 end)

    %__MODULE__{
      handles: handles
    }
  end

  def from_payload(<<>>, _opts) do
    %__MODULE__{}
  end

  def payload(%__MODULE__{handles: handles}, _opts) do
    handles
    |> Enum.reduce(<<>>, fn {k, v}, acc ->
      acc <> <<k::big-16, byte_size(v)::big-16>> <> v
    end)
  end

  defmodule Ack do
    alias __MODULE__

    defstruct results: %{}

    def type do
      21
    end

    def from_payload(<<bin::binary>>, _opts) do
      results =
        parse_payload(bin, [])
        |> Map.new()

      %Ack{results: results}
    end

    def payload(%Ack{results: results}, _opts) do
      results
      |> Enum.reduce(<<>>, fn {id, r}, acc -> acc <> <<id::big-16, r>> end)
    end

    def add_result(%Ack{results: results} = ack, handle, result) do
      %Ack{ack | results: Map.put(results, handle, result)}
    end

    def get_result(%Ack{results: results}, handle) do
      Map.get(results, handle)
    end

    defp parse_payload(<<>>, acc) do
      acc
    end

    defp parse_payload(<<id::big-16, r, rest::binary>>, acc) do
      parse_payload(rest, acc ++ [{id, r}])
    end
  end
end
