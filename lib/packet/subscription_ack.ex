defmodule Lighttel.Packet.Subscription.Ack do
  @behaviour Lighttel.Packet

  defstruct results: %{}

  alias __MODULE__

  @impl true
  def type do
    31
  end

  @spec add(%Ack{}, String.t() | integer, integer) :: %Ack{}
  def add(%Ack{results: results}, endpoint, result) do
    %Ack{results: Map.put(results, endpoint, result)}
  end

  def add_success(ack, endpoint) do
    add(ack, endpoint, 0)
  end

  def add_failure(ack, endpoint) do
    add(ack, endpoint, 0x80)
  end

  @doc """
  Converts a binary to an Ack result

  ## Examples

      iex>Lighttel.Packet.Subscription.Ack.from_payload <<0, 1, 0>> <> <<0, 0, 5 :: big-16, "hello", 0x80>>, []
      %Lighttel.Packet.Subscription.Ack{results: %{1 => 0, "hello" => 0x80}}

  """
  @impl true
  def from_payload(<<data::binary>>, _opts) do
    %Ack{results: parse_payload(data, %{})}
  end

  defp parse_payload(<<>>, acc) do
    acc
  end

  defp parse_payload(
         <<0::big-16, ep_len::big-16, ep::binary-size(ep_len), res, rest::binary>>,
         acc
       ) do
    parse_payload(rest, Map.put(acc, ep, res))
  end

  defp parse_payload(<<h::big-16, r, rest::binary>>, acc) when h > 0 do
    parse_payload(rest, Map.put(acc, h, r))
  end

  @doc """
  Creates binary payload from an Ack structure

  ## Examples

      iex>Lighttel.Packet.Subscription.Ack.payload %Lighttel.Packet.Subscription.Ack{results: %{1 => 0, "hello" => 0x80}}, []
      <<0, 1, 0>> <> <<0, 0, 5 :: big-16, "hello", 0x80>>

  """
  @impl true
  def payload(%Ack{results: results}, _opts) do
    for res <- results, into: <<>>, do: to_payload(res)
  end

  defp to_payload({h, r}) when is_integer(h) and h > 0 do
    <<h::big-16, r>>
  end

  defp to_payload({h, r}) when is_binary(h) do
    <<0::big-16>> <> Lighttel.String.to_binary!(h) <> <<r>>
  end
end
