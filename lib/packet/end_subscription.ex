defmodule Lighttel.Packet.EndSubscription do
  @behaviour Lighttel.Packet

  alias __MODULE__

  defstruct endpoints: %MapSet{}

  defmodule Ack do
    @behaviour Lighttel.Packet
    alias __MODULE__
    alias Lighttel.Packet.EndSubscription

    defstruct endpoints: %MapSet{}

    @impl true
    def type do
      34
    end

    @doc """
    Creates an EndSubscription packet  from a binary payload

    ## Examples

        iex>Lighttel.Packet.EndSubscription.Ack.from_payload <<0, 0, 0, 5, "hello", 0, 1>>, []
        %Lighttel.Packet.EndSubscription.Ack{endpoints: MapSet.new(["hello", 1])}

    """
    @impl true
    def from_payload(bin, opts) do
      %EndSubscription{endpoints: eps} = EndSubscription.from_payload(bin, opts)
      %Ack{endpoints: eps}
    end

    @doc """
    Creates a binary representation of an end subscription packet

    ## Examples

        iex>Lighttel.Packet.EndSubscription.Ack.payload(%Lighttel.Packet.EndSubscription.Ack{endpoints: MapSet.new(["hello", 1])}, [])
        <<0, 1, 0, 0, 0, 5, "hello">>

    """
    @impl true
    def payload(%Ack{endpoints: eps}, opts) do
      EndSubscription.payload(%EndSubscription{endpoints: eps}, opts)
    end
  end

  @impl true
  def type do
    33
  end

  @doc """
  Creates an EndSubscription packet  from a binary payload

  ## Examples

      iex>Lighttel.Packet.EndSubscription.from_payload <<0, 0, 0, 5, "hello", 0, 1>>, []
      %Lighttel.Packet.EndSubscription{endpoints: MapSet.new(["hello", 1])}

  """
  @impl true
  def from_payload(<<bin::binary>>, _opts) do
    parse_payload(bin, %EndSubscription{})
  end

  defp parse_payload(<<>>, acc) do
    acc
  end

  defp parse_payload(<<b::binary>>, acc) do
    {ep, rest} =
      case b do
        <<0::big-16, ep_len::big-16, ep::binary-size(ep_len), rest::binary>> -> {ep, rest}
        <<ep::big-16, rest::binary>> -> {ep, rest}
      end

    parse_payload(rest, add(acc, ep))
  end

  @doc """
  Creates a binary representation of an end subscription packet

  ## Examples

      iex>Lighttel.Packet.EndSubscription.payload(%Lighttel.Packet.EndSubscription{endpoints: MapSet.new(["hello", 1])}, [])
      <<0, 1, 0, 0, 0, 5, "hello">>

  """
  @impl true
  def payload(%EndSubscription{endpoints: eps}, _opts) do
    for ep <- eps, into: <<>> do
      case is_integer(ep) do
        true -> <<ep::big-16>>
        false -> <<0::big-16>> <> Lighttel.String.to_binary!(ep)
      end
    end
  end

  def add(%EndSubscription{endpoints: eps}, ep) when is_integer(ep) and ep !== 0 do
    %EndSubscription{endpoints: MapSet.put(eps, ep)}
  end

  def add(%EndSubscription{endpoints: eps}, ep) when is_binary(ep) and byte_size(ep) > 0 do
    %EndSubscription{endpoints: MapSet.put(eps, ep)}
  end

  def delete(%EndSubscription{endpoints: eps}, ep) do
    %EndSubscription{endpoints: MapSet.delete(eps, ep)}
  end
end
