defmodule Lighttel.Packet.Ping do
  @behaviour Lighttel.Packet

  @moduledoc """
  Defines the lighttel `ping` packet.

  """

  # Declare a single variable in the struct that is unused
  defstruct _unused: nil

  @packet_type 40

  def type do
    @packet_type
  end

  def from_payload(<<>>, _opts) do
    %Lighttel.Packet.Ping{}
  end

  def payload(_, _) do
    <<>>
  end
end
