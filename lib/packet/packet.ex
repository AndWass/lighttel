defmodule Lighttel.Packet do
  @moduledoc """
  Contains packet-specific operations.

  # Packet-types

  Two types of packets are discussed in the documentation:
  * generic packets
  * specific packets

  **Generic packets** are described by the generic `Lighttel.Packet` struct
  and can contain both valid and invalid lighttel packets.

  **Specific packets** are described by the corresponding `Lighttel.Packet.*` module(s).
  For instance `%Lighttel.Packet.Connect{}` is a specific packet, while `%Lighttel.Packet{}` is
  a generic packet.

  # Defining specific packets
  To define a specific packet do the following:
  * Define a module and a struct inside the module
  * Add the following functions to the module: `type/0`, `from_payload/1` and `payload/1`
  * Add the module to `@specific_packet_types` list

  See `Lighttel.Packet.Ping` for the definition of a minimal specific packet.

  """
  defstruct header: %Lighttel.Packet.FixedHeader{}, data: <<>>

  @typedoc """
  Parse-options is either an empty or a non-empty list.
  """

  @type parse_options :: [] | [...]

  @callback type() :: integer
  @callback from_payload(binary, parse_options) :: map
  @callback payload(map, parse_options) :: binary

  alias __MODULE__

  @specific_packet_types [
    Lighttel.Packet.Connect,
    Lighttel.Packet.Connect.Ack,
    Lighttel.Packet.PutPublishHandle,
    Lighttel.Packet.PutPublishHandle.Ack,
    Lighttel.Packet.PutAuth,
    Lighttel.Packet.PutAuth.Ack,
    Lighttel.Packet.PutAuth.Nack,
    Lighttel.Packet.Publish,
    Lighttel.Packet.BatchPublish,
    Lighttel.Packet.Publish.Ack,
    Lighttel.Packet.Subscription,
    Lighttel.Packet.Subscription.Ack,
    Lighttel.Packet.EndSubscription,
    Lighttel.Packet.EndSubscription.Ack,
    Lighttel.Packet.Ping
  ]

  @doc """
  Parses a complete packet from a stream of binary data. Returns either an error
  if more data is needed or `{:ok, packet, next_packet_data}`

  ## Examples

      iex>Lighttel.Packet.from_binary(<<1,8,1,4,5,6,7,6,7,0>>)
      {
          :ok,
          %Lighttel.Packet.Connect
          {
              batch_publish: false,
              connection_idle_timeout: 1543,
              compression_support: [],
              maximum_packet_size: 4116,
              protocol_version: 1,
              req_remote_connection_idle_timeout: 1543
          },
          <<>>
      }

      iex>Lighttel.Packet.from_binary(<<1,9,3>>)
      {:error, :need_more_data, <<1,9,3>>}

      iex>Lighttel.Packet.from_binary(<<1,0x81,0x80,0x80,0x80,0x80>>)
      {:error, :bad_packet}

  """
  @spec from_binary(binary, parse_options) ::
          {:ok, map} | {:error, :need_more_data, binary} | term
  def from_binary(<<bin::binary>>, opts \\ []) do
    with {:ok, header, rest} <- Lighttel.Packet.FixedHeader.from_binary(bin),
         true <- byte_size(rest) >= header.remaining_length || {:error, :need_more_data, bin},
         packet_data <- :binary.part(rest, 0, header.remaining_length),
         next_packet_data <-
           :binary.part(rest, header.remaining_length, byte_size(rest) - header.remaining_length),
         {:ok, specific_packet} <-
           to_specific_packet(%Lighttel.Packet{header: header, data: packet_data}, opts),
         do: {:ok, specific_packet, next_packet_data}
  end

  @spec from_binary!(binary, parse_options) :: {map, binary}
  def from_binary!(bin, opts \\ []) do
    {:ok, header, rest} = from_binary(bin, opts)
    {header, rest}
  end

  @doc """
  Takes a generic packet and creates a specific packet.
  """
  @spec to_specific_packet(map, parse_options) :: {:ok, map}
  def to_specific_packet(packet = %Packet{}, opts \\ []) do
    packet_module = type_to_module(packet.header.type)

    specific_packet = packet_module.from_payload(packet.data, opts)
    {:ok, specific_packet}
  end

  @spec type_to_module(integer) :: atom
  defp type_to_module(type) do
    Enum.find(@specific_packet_types, &(&1.type() == type))
  end

  @doc """
  Converts a packet to binary data

  ##Examples

      iex>Lighttel.Packet.to_binary(%Lighttel.Packet{})
      {:ok, <<0,0>>}

      iex>Lighttel.Packet.to_binary(%Lighttel.Packet{header: %Lighttel.Packet.FixedHeader{type: 3, remaining_length: 4}, data: <<1,2,5,6>>})
      {:ok, <<3,4,1,2,5,6>>}

      iex>Lighttel.Packet.to_binary(%Lighttel.Packet{header: %Lighttel.Packet.FixedHeader{type: 3, remaining_length: 4}, data: <<>>})
      {:error, :bad_packet}

      iex>Lighttel.Packet.to_binary(%Lighttel.Packet.Connect{})
      {:ok, <<1, 8, 1, 2, 0, 1, 44, 1, 44, 1>>}

  """
  @spec to_binary(map, parse_options) :: {:ok, binary} | {:error, atom} | term
  def to_binary(packet, opts \\ [])

  def to_binary(packet = %Lighttel.Packet{}, _opts) do
    with :ok <- validate_packet_length(packet),
         {:ok, header_bin} <- Lighttel.Packet.FixedHeader.to_binary(packet.header),
         do: {:ok, header_bin <> packet.data}
  end

  def to_binary(packet, opts) when is_map(packet) do
    {:ok, generic_packet} = to_generic_packet(packet, opts)
    to_binary(generic_packet)
  end

  @spec to_binary!(map, parse_options) :: binary
  def to_binary!(packet, opts \\ []) do
    {:ok, bin} = to_binary(packet, opts)
    bin
  end

  @doc """
  Converts a `packet` to a generic packet. Generic packets
  will cause no conversion to take place, while specific packets will
  convert the specific packet to a generic one.

  """
  @spec to_generic_packet(map, parse_options) :: {:ok, map}
  def to_generic_packet(packet, opts \\ [])

  def to_generic_packet(packet = %Packet{}, _opts) do
    {:ok, packet}
  end

  def to_generic_packet(%mod{} = packet, opts) do
    type = mod.type()
    data = mod.payload(packet, opts)
    data_len = byte_size(data)

    {:ok,
     %Packet{header: %Packet.FixedHeader{type: type, remaining_length: data_len}, data: data}}
  end

  @spec to_generic_packet!(map, parse_options) :: map
  def to_generic_packet!(packet, opts \\ []) do
    {:ok, p} = to_generic_packet(packet, opts)
    p
  end

  @spec ui16_to_binary(integer) :: binary
  def ui16_to_binary(ui) when is_integer(ui) do
    <<ui::big-16>>
  end

  @spec validate_packet_length(map) :: :ok | {:error, atom}
  defp validate_packet_length(packet) do
    if packet.header.remaining_length == byte_size(packet.data) do
      :ok
    else
      {:error, :bad_packet}
    end
  end
end
