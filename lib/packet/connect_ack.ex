defmodule Lighttel.Packet.Connect.Ack do
  @behaviour Lighttel.Packet

  @moduledoc """
  Defines the `connect ack` packet.

  """
  defstruct protocol_version: 1,
            # Maximum packet size in bytes. On the wire this/4 is sent.
            maximum_packet_size: 2048,
            connection_idle_timeout: 300,
            compression_support: []

  @packet_type 2

  @doc """
  Gets the type parameter for the fixed header
  """
  def type do
    @packet_type
  end

  @doc """
  Parses binary data and returns a connect packet if the data is well-formed

  ##Examples

      iex>Lighttel.Packet.Connect.Ack.from_payload(
      ...> data: <<1, 512 :: big-16, 300 :: big-16, 0>>)
      %Lighttel.Packet.Connect.Ack{}

  """
  def from_payload(
        <<1, maximum_packet_size::big-16, connection_idle_timeout::big-16, compression_support>>,
        _opts
      ) do
    packet = %Lighttel.Packet.Connect.Ack{
      maximum_packet_size: 4 * maximum_packet_size,
      connection_idle_timeout: connection_idle_timeout,
      compression_support: Lighttel.Compression.bitfield_to_algorithms(compression_support)
    }

    packet
  end

  @doc """
  Serializes a connect packet to binary.

  ##Examples

      iex>Lighttel.Packet.Connect.Ack.payload(%Lighttel.Packet.Connect.Ack{})
      <<1, 512 :: big-16, 300 :: big-16, 0>>

  """
  def payload(
        %Lighttel.Packet.Connect.Ack{
          protocol_version: protocol_version,
          maximum_packet_size: maximum_packet_size,
          connection_idle_timeout: connection_idle_timeout,
          compression_support: compression_support
        },
        _opts
      ) do
    wire_packet_size = Integer.floor_div(maximum_packet_size, 4)

    <<protocol_version, wire_packet_size::big-16, connection_idle_timeout::big-16,
      Lighttel.Compression.algorithms_to_bitfield(compression_support)>>
  end
end
