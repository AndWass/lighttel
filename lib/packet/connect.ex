defmodule Lighttel.Packet.Connect do
  @behaviour Lighttel.Packet

  @moduledoc """
  Defines the lighttel `connect` packet.

  """

  @packet_type 1

  defstruct protocol_version: 1,
            # Maximum packet size in bytes. On the wire this/4 is sent.
            maximum_packet_size: 2048,
            connection_idle_timeout: 300,
            req_remote_connection_idle_timeout: 300,
            compression_support: Lighttel.Compression.algorithms(),
            batch_publish: false

  def type do
    @packet_type
  end

  def payload(
        %Lighttel.Packet.Connect{
          protocol_version: protocol_version,
          maximum_packet_size: maximum_packet_size,
          connection_idle_timeout: connection_idle_timeout,
          req_remote_connection_idle_timeout: req_remote_connection_idle_timeout,
          compression_support: compression_support,
          batch_publish: batch_publish
        },
        _opts
      ) do
    wire_packet_size = Integer.floor_div(maximum_packet_size, 4)
    compression_bitfield = Lighttel.Compression.algorithms_to_bitfield(compression_support)
    batch_publish = if batch_publish, do: 1, else: 0

    <<protocol_version, wire_packet_size::big-16, connection_idle_timeout::big-16,
      req_remote_connection_idle_timeout::big-16, batch_publish::size(1), 0::size(6),
      compression_bitfield::size(1)>>
  end

  def from_payload(
        <<1, maximum_packet_size::big-16, connection_idle_timeout::big-16,
          req_remote_connection_idle_timeout::big-16, batch_publish::size(1), 0::size(6),
          compression_support::size(1)>>,
        _opts
      ) do
    packet = %Lighttel.Packet.Connect{
      maximum_packet_size: 4 * maximum_packet_size,
      connection_idle_timeout: connection_idle_timeout,
      req_remote_connection_idle_timeout: req_remote_connection_idle_timeout,
      compression_support: Lighttel.Compression.bitfield_to_algorithms(compression_support),
      batch_publish: if(batch_publish == 1, do: true, else: false)
    }

    packet
  end
end
