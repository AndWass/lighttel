defmodule Lighttel.Packet.Publish.Ack do
  @behaviour Lighttel.Packet

  alias __MODULE__

  defstruct message_ids: []

  def type do
    25
  end

  def from_payload(<<data::binary>>, _opts) do
    %Ack{
      message_ids: parse_payload!(data, [])
    }
  end

  def payload(%Ack{message_ids: ids}, _opts) do
    for id <- ids, into: <<>>, do: <<id::big-16>>
  end

  defp parse_payload!(<<>>, acc) do
    acc
  end

  defp parse_payload!(<<id::big-16, rest::binary>>, acc) do
    parse_payload!(rest, acc ++ [id])
  end
end
