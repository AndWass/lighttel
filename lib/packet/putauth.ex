defmodule Lighttel.Packet.PutAuth do
  @behaviour Lighttel.Packet

  @moduledoc """
  Defines the lighttel `putauth` packet.

  """

  defstruct handle: 0,
            auth_method: :userpass,
            auth_data: %{
              :username => "",
              :password => ""
            }

  alias __MODULE__

  def type do
    10
  end

  def auth_methods do
    [:userpass]
  end

  def numeric_to_auth_method(x) do
    Enum.at(auth_methods(), x)
  end

  def auth_method_to_numeric(x) do
    Enum.find_index(auth_methods(), fn y -> x === y end)
  end

  @doc """
  Builds a putauth packet from a binary payload.
  ## Examples

        iex>Lighttel.Packet.PutAuth.from_payload(<<0,0, 0, 0,4,"user", 0, 4, "pass">>, [])
        %Lighttel.Packet.PutAuth{
            handle: 0,
            auth_method: :userpass,
            auth_data: %{
                :username => "user",
                :password => "pass"
            }
        }

        iex>Lighttel.Packet.PutAuth.from_payload(<<1,2, 0, 0, 0, 0, 0>>, [])
        %Lighttel.Packet.PutAuth{
            handle: 258,
            auth_method: :userpass,
            auth_data: %{
              :username => "",
              :password => ""
            }
        }

  """
  def from_payload(<<handle::big-16, 0::size(7), auth_method::size(1), auth_data::binary>>, _opts) do
    auth_method = numeric_to_auth_method(auth_method)

    packet = %PutAuth{
      handle: handle,
      auth_method: auth_method,
      auth_data:
        if auth_method === :userpass do
          {:ok, username, rest} = Lighttel.String.from_binary(auth_data)
          {:ok, password, _} = Lighttel.String.from_binary(rest)

          %{
            :username => username,
            :password => password
          }
        else
          %{}
        end
    }

    packet
  end

  @doc """
  Converts a packet to binary payload
  ## Examples

        iex>Lighttel.Packet.PutAuth.payload(%Lighttel.Packet.PutAuth{}, [])
        <<0, 0, 0, 0, 0, 0, 0>>

  """
  def payload(packet = %PutAuth{}, _opts) do
    packet_auth_data =
      if packet.auth_method === :userpass do
        username = Map.get(packet.auth_data, :username)
        password = Map.get(packet.auth_data, :password)
        Lighttel.String.to_binary!(username) <> Lighttel.String.to_binary!(password)
      else
        <<>>
      end

    <<packet.handle::big-16, auth_method_to_numeric(packet.auth_method)>> <> packet_auth_data
  end
end
