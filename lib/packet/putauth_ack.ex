defmodule Lighttel.Packet.PutAuth.Ack do
  @behaviour Lighttel.Packet

  alias __MODULE__

  defstruct handle: 0,
            valid_for: 0

  def type do
    11
  end

  def from_payload(<<handle::big-16, valid_for::big-16>>, _opts) do
    %Ack{
      handle: handle,
      valid_for: valid_for
    }
  end

  def payload(
        %Ack{
          handle: handle,
          valid_for: valid_for
        },
        _opts
      ) do
    <<handle::big-16, valid_for::big-16>>
  end
end
