defmodule Lighttel.PublishTest do
  use ExUnit.Case, async: true
  doctest Lighttel.Packet.Publish

  test "Test compression" do
    {:deflate, compressed_data} = Lighttel.Compression.compress("world", :deflate)

    assert Lighttel.Packet.Publish.payload(
             %Lighttel.Packet.Publish{
               message_id: 257,
               endpoint: "hello",
               payload: "world"
             },
             compression: :deflate
           ) === <<257::big-16, 0x81, 5::big-16, "hello", 0>> <> compressed_data
  end

  test "Test decompression" do
    {_, compressed_data} =
      Lighttel.Compression.compress(<<0, 5, "hello", 0, 5, "world", "1,2,3">>, :deflate)

    assert Lighttel.Packet.Publish.from_payload(
             <<257::big-16, 0x81, 5::big-16, "hello", 2 * 2 + 10>> <> compressed_data,
             []
           ) ===
             %Lighttel.Packet.Publish{
               message_id: 257,
               endpoint: "hello",
               payload: "1,2,3",
               headers: %{"hello" => ["world"]}
             }
  end
end
